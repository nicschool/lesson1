//
//  ResultViewController.swift
//  Lesson1
//
//  Created by Nithi Kulasiriswatdi on 5/8/2564 BE.
//

import UIKit

class ResultViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    var user: User?
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = user?.name

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
