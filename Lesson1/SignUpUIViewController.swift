//
//  SignUpUIViewController.swift
//  Lesson1
//
//  Created by Peerapong Thongpubet on 5/8/2564 BE.
//

import UIKit
class SignUpUIViewController:
    UIViewController {

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var nicknameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var ageText: UITextField!
    @IBOutlet weak var sexText: UISegmentedControl!
    
    @IBOutlet weak var passwordConfirmText: UITextField!
    
    @IBOutlet weak var signup: UIButton!
    override func viewDidLoad() {
    super.viewDidLoad()
        passwordText.isSecureTextEntry = true
        passwordConfirmText.isSecureTextEntry = true
    
        // Do any additional setup after loading the view.
    }
    @IBAction func editNameField(_ sender: UITextField) {
        
    }
    
    @IBAction func pushConfirmButton(_ sender: Any) {
        
    }
    @IBAction func editUsername(_ sender: UITextField) {
       
    }
    
    
        @IBAction func SingupPush(_ sender: Any) {
   //     userInfo.email=emailText.text
   //     userInfo.username=usernameText.text
    }
    
    @IBAction func signupSubmit(_ sender: UIButton) {
        signup.backgroundColor = UIColor.red
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let destination = segue.destination as? SignupInfoViewController,
           let userName = username.text,
           let nameString = usernameText.text,
           let nickName = nicknameText.text,
           let emailString = emailText.text,
           let gender = Gender(rawValue: sexText.selectedSegmentIndex),
           let age = Int(ageText.text ?? ""){
            let user = User(username: nameString,
                            name: userName,
                            nickName: nickName,
                            age: age,
                            sex: gender,
                            email: emailString)
            destination.user = user
        }
    }
    

}
