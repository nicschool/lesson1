//
//  UserContext.swift
//  Lesson1
//
//  Created by Nithi Kulasiriswatdi on 19/8/2564 BE.
//

import Foundation
enum Gender: Int {
    case Male = 0
    case Female = 1
}
struct User {
    let username: String
    let name: String
    let nickName: String
    let age: Int
    let sex: Gender
    let email: String
}
class UserContext {
    static var userList: [User] = []
}
