//
//  UserCell.swift
//  Lesson1
//
//  Created by Nithi Kulasiriswatdi on 19/8/2564 BE.
//

import UIKit

class UserCell: UITableViewCell {
    @IBOutlet weak var emailLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setup(email: String) {
        emailLabel.text = email
    }

}
