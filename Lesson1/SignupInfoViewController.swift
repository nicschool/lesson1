//
//  SignupInfoViewController.swift
//  Lesson1
//
//  Created by Peerapong Thongpubet on 10/8/2564 BE.
//

import UIKit

class SignupInfoViewController: UIViewController {
    var user: User?

    var passwordString: String = ""
    @IBOutlet weak var infoLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        infoLabel.numberOfLines = 0
        var displaytext = ""
        guard let user = user else {
            return
        }
        displaytext = "Username: \(user.username)\n"
        displaytext += "Name: \(user.name)\n"
        displaytext += "Email: \(user.email)\n"
        displaytext += "Age: \(user.age)\n"
        displaytext += "Gender: \(user.sex)\n"
        infoLabel.text = displaytext
    }

}
