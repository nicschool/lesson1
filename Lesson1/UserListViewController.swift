//
//  UserListViewController.swift
//  Lesson1
//
//  Created by Nithi Kulasiriswatdi on 19/8/2564 BE.
//

import UIKit

class UserListViewController: UIViewController, UITableViewDataSource{
  
    @IBOutlet weak var tableView: UITableView!
 
    @IBAction func pressAddNewUsers(_ sender: Any) {
        let user = User(username: "Bhumibol",
                        name: "",
                        nickName: "",
                        age: 88,
                        sex: .Female,
                        email: "")
        UserContext.userList.append(user)
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        

        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserContext.userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user = UserContext.userList[indexPath.row]
        let userCell = tableView.dequeueReusableCell(withIdentifier: "UserCellIdentifier") as! UserCell
        userCell.setup(email: user.username)
        return userCell
    }
}

/*
 let userCell = UserCell()
 let emailLabel = UILabel(frame: CGRect(x: 10, y: 10, width: 100, height: 20))
 emailLabel.text = "kuy"
 userCell.contentView.addSubview(emailLabel)
 return userCell*/
