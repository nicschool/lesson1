//
//  SignupViewController.swift
//  Lesson1
//
//  Created by Nithi Kulasiriswatdi on 5/8/2564 BE.
//

import UIKit

struct User {
    let name: String
    let password: String
}
class SignupViewController: UIViewController {

    @IBOutlet weak var submitButton: UIButton!
    private var userName: String?
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    @IBAction func pressSubmit(_ sender: UIButton) {
        
        
    }
    @IBAction func userNameValueChanged(_ sender: UITextField) {
        userName = sender.text
    }
    

  
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if let destination = segue.destination as? ResultViewController ,
           let name = userName{
           let user = User(name: name,
                           password: "")
            destination.user = user
        }
    }
 

}
