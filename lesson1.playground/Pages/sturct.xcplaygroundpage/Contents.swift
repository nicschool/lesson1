import UIKit



class StudentClass {
    var name: String
    var age: Int
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
   
}

struct Student {
    var name: String
    var age: Int
    func yed() {
        
    }
}

var student1 = Student(name: "Nic", age: 32)
var student2 = student1
student2.age = 28

print(student2)
