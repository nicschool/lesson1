

var a = 1

a = 2
a = 3
a = 1+1

func plusza(a: Int,b: Int) -> Int {
    return a + b
}

func teachMeClass() {
    class Head {
        var eye: Int?
        var nose: String?
    }

    class Human {
        var age: Int
        var height: Int

        func displayAge() {
            print("คนนี้อายุ \(age) ปี")
        }
        func displayHeight() {
            
            print("คนนี้สูง \(height) เซน")
        }
        init(age:Int , height: Int) {
            self.age = age
            self.height = height
        }

    }

    let vachira = Human(age: 70, height: 170)
    vachira.displayAge()
    vachira.displayHeight()

    let bhumibol = vachira
    bhumibol.age = 0
    bhumibol.displayAge()
    vachira.displayAge()



    struct Humanz {
        var age: Int
        var height: Int
        func displayAge() {
            print("คนนี้อายุ \(age) ปี")
        }
        func displayHeight() {

            print("คนนี้สูง \(height) เซน")
        }

    }

    let suthida = Humanz(age: 40, height: 165)
    suthida.displayAge()
    suthida.displayHeight()

    var koy = suthida
    koy.age = 38
    koy.displayAge()
    suthida.displayAge()
}

func BaseClass() {
    class School {
        let phoor = "ปลาหยุด"
        func study() {
            print("ฉันชอบเรียนหนังสือ")
        }
        func sleep() {
            print("ง่วงโว้ย")
        }
    }

    class Bodindecha: School {
        func start() {
            study()
            sleep()
            print(phoor)
        }
        override func study() {
            super.study()
            print("ไม่ชอบเรียน")
        }
    }


    let nicSchool = Bodindecha()
    nicSchool.start()
}

protocol Kitchen {
    func getItemFromFridge() -> String
    func getCookingAction() -> String
}

class Restuarant {
    var delegate: Kitchen?
    func cook() {
        let action = delegate?.getCookingAction()
        let item = delegate?.getItemFromFridge()
        print("พ่อครัวกำลังจะ \(action) \(item) นะจ๊ะ")
    }
}


class KFC: Kitchen {
    let restuarant = Restuarant()
    init() {
        restuarant.delegate = self
        restuarant.cook()
    }
    func getItemFromFridge() -> String {
        print("KFC กำลังจะส่งของ")
        return "ไก่"
    }

    func getCookingAction() -> String {
        print("KFC กำลังจะทำบางอย่าง")
        return "ทอด"
    }
}

