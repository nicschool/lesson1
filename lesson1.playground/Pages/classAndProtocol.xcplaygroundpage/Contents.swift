
protocol Kitchen {
    func getItem() -> String
    func action() -> String
}

class Restuarant {
    var delegate: Kitchen?
    func cook() {
        let item = delegate?.getItem()
        let action = delegate?.action()
        print("\(action) \(item)")
    }
    
}

class KFC: Kitchen {
    let restuarant = Restuarant()
    func start() {
//        restuarant.delegate = self
        restuarant.cook()
    }
    func getItem() -> String {
        return "Chicken"
    }
    func action() -> String {
        return "Fried"
    }
}

class MaxBeef: Kitchen {
    let restuarant = Restuarant()
    func start() {
        restuarant.delegate = self
        restuarant.cook()
    }
    func getItem() -> String {
        return "Beef"
    }
    func action() -> String {
        return "Grill"
    }
}


KFC().start()
MaxBeef().start()
